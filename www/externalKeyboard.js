/**
 * cordova-plugin-external-keyboard
 * 외부 키보드 감지 플러그인
 */

    var argscheck = require('cordova/argscheck'),
       utils = require('cordova/utils'),
       exec = require('cordova/exec');

    var ExternalKeyboard = function() {};

    // 외부 키보드 사용 여부 (초기 : false)
    ExternalKeyboard.isUsed = false;

    // 콜백 없이 사용여부 체크만 수행
    /**
     * deviceready 이후 가능
     * ex) ExternalKeyboard.checkOnce();
     */
    ExternalKeyboard.checkOnce = function() {
       var self = this;

       exec(function(winParam){
            self.isUsed = winParam;
            console.log(self.isUsed);
            },
            function(error){ console.log(error); },
            "ExternalKeyboard",
            "isExternalKeyboardAttached",
            []
            );
       }

    // 키보드 검사 후 콜백 실행
    /**
     * deviceready 이후 가능
     * ex) ExternalKeyboard.check({
     *       use: function() { ... },
     *       disuse: function() { ... }
     *     });
     */
    ExternalKeyboard.check = function(arg) {
       var self = this;
       //  use : 외부 키보드 있을 경우 수행
       //  disuse : 외부 키보드 없을 경우 수행
       var action = Object.assign({},
           {
              use : function() { console.log("use func not exist"); },
              disuse : function() { console.log("disuse func not exist"); }
           },
           arg||{});


        exec(function(winParam){
             // 연동 성공 시 수행 부분
             self.isUsed = winParam;
             console.log(self.isUsed);
             if (self.isUsed) {
               // 외부키보드 있을 경우
                 typeof action.use === "function" && action.use();
             } else {
               // 외부키보드 없을 경우
                 typeof action.disuse === "function" && action.disuse();
             }
         },
         function(error){ console.log(error); },
         "ExternalKeyboard",
         "isExternalKeyboardAttached",
         []
         );
    }

    module.exports = ExternalKeyboard;
