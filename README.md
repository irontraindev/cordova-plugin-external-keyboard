# README #

보카트레인 외부 키보드 검사 플러그인입니다.

### 설치 ###

* 코르도바 프로젝트 폴더에서 입력합니다.
* 설치 시
```javascript
cordova plugin add git+ssh://git@bitbucket.org:irontraindev/cordova-plugin-external-keyboard.git
```
* 삭제 시
```javascript
cordova plugin remove cordova-plugin-external-keyboard
```

### 사용 ###

* 외부 키보드 사용여부 값을 가져옵니다. (Boolean)
```javascript
ExternalKeyboard.isUsed
```
* 외부 키보드 검사 후 콜백 함수 use, disuse 를 실행합니다.
```javascript
ExternalKeyboard.check({
   use: function() { /* 외부 키보드 있을 경우 */ },
   disuse: function() { /* 외부 키보드 없을 경우 */ }
});
```
* 콜백 함수 없이 체크 후 isUsed를 업데이트 합니다.
```javascript
ExternalKeyboard.checkOnce();
```

### 보카트레인 iOS 기타 호환 사항 ###

* cordova-plugin-keyboard 를 설치 후 적용
```javascript
// index.js 410줄
// index.html 1134줄
// 키보드 플러그인을 써도 ios에서는 show 메소드가 작동은 안하지만 에러는 발생하지 않음
KeyBoard.showKeyboard() --> Keyboard.show();

// index.html 1132 줄
// hide 메소드는 ios에서도 작동 함
KeyBoard.hideKeyboard(); --> Keyboard.hide();
```

* Content-Security-policy 의 default-src 에 gap://ready 추가
```javascript
// index.html 26 줄
<meta http-equiv="Content-Security-Policy" content="default-src * gap://ready; style-src 'self' 'unsafe-inline'; script-src 'self' 'unsafe-inline' 'unsafe-eval'">
```

### iOS 외부 키보드 이슈 ###

* input 외부에서 외부 키보드 작동하지 않음
* document, window 객체에 keydown 등 이벤트 바인딩해도 응답없음 (실제 아이폰도 동일)
* input의 focus() 메소드 동작을 위해 아래와 같이 true -> false 로 변경해야함
```javascript
// config.xml
<preference name="KeyboardDisplayRequiresUserAction" value="false" />
```
* input의 style 속성이 display: none; / visibility: hidden; 이면 인식하지 못하여 외부 키보드를 사용할 수 없음
