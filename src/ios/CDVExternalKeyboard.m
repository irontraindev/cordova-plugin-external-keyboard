/**
 * cordova-plugin-external-keyboard
 * 외부 키보드 감지 플러그인
 */

#import "CDVExternalKeyboard.h"
#import <Cordova/CDV.h>
#import <objc/message.h>

@implementation CDVExternalKeyboard

// direct check for external keyboard
- (void) isExternalKeyboardAttached:(CDVInvokedUrlCommand *)command
{
    // 외부 키보드 사용여부 반환 값
    BOOL externalKeyboardAttached = NO;
    // 키보드 사용 판별 과정 중 결과 나오면 YES
    BOOL processEnd = NO;

    @try {
        NSString *keyboardClassName = [@[@"UI", @"Key", @"boa", @"rd", @"Im", @"pl"] componentsJoinedByString:@""];
        Class c = NSClassFromString(keyboardClassName);
        SEL sharedInstanceSEL = NSSelectorFromString(@"sharedInstance");
        if (c == Nil || ![c respondsToSelector:sharedInstanceSEL]) {
            externalKeyboardAttached = NO;
            processEnd = YES;
        }

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        id sharedKeyboardInstance = [c performSelector:sharedInstanceSEL];
#pragma clang diagnostic pop

        if (!processEnd && ![sharedKeyboardInstance isKindOfClass:NSClassFromString(keyboardClassName)]) {
            externalKeyboardAttached = NO;
            processEnd = YES;
        }

        NSString *externalKeyboardSelectorName = [@[@"is", @"InH", @"ardw", @"areK", @"eyb", @"oard", @"Mode"] componentsJoinedByString:@""];
        SEL externalKeyboardSEL = NSSelectorFromString(externalKeyboardSelectorName);
        if (!processEnd && ![sharedKeyboardInstance respondsToSelector:externalKeyboardSEL]) {
            externalKeyboardAttached = NO;
            processEnd = YES;
        }

        if (!processEnd) {
            externalKeyboardAttached = ((BOOL ( *)(id, SEL))objc_msgSend)(sharedKeyboardInstance, externalKeyboardSEL);
        }
    } @catch(__unused NSException *ex) {
        externalKeyboardAttached = NO;
    }
    // cordova plugin 연동
    CDVPluginResult*  pluginResult = [ CDVPluginResult
                                      resultWithStatus    : CDVCommandStatus_OK
                                      messageAsBool:externalKeyboardAttached
                                      ];
    // 결과 반환
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end
