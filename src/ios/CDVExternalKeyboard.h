/**
 * cordova-plugin-external-keyboard
 * 외부 키보드 감지 플러그인
 */

#import <Cordova/CDV.h>
// 외부 키보드 인터페이스
@interface CDVExternalKeyboard : CDVPlugin

- (void) isExternalKeyboardAttached:(CDVInvokedUrlCommand *)command;

@end
